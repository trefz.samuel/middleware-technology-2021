package de.hft.stuttgartMiddlewareTechnology.middlewareTechnology;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiddlewareTechnologyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiddlewareTechnologyApplication.class, args);
	}

}
